//
//  File.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

protocol AnimationObserverDelegate {
    func animationUpdatedTo(progress: CGFloat)
}

class AnimationObserver : CALayer, AnimationObserverDelegate {
    private var progress : CGFloat = 0
    private var progressObserverClosure : ((CGFloat)->Void)?
    var progressDelegate : AnimationObserverDelegate? = nil
    
    override class func needsDisplay(forKey key: String) -> Bool {
        if key == "progress" {
            return true
        } else {
            return super.needsDisplay(forKey: key)
        }
    }
    
    override func draw(in ctx: CGContext) {
        progressDelegate?.animationUpdatedTo(progress: progress)
    }
    
    func animateWith(from:Double, to:Double, duration: Double, options: UIViewAnimationOptions, progress:((CGFloat)->Void)?) {
        progressObserverClosure = progress
    
        let anim = CABasicAnimation(keyPath: "progress")
        anim.duration = 4.0;
        anim.beginTime = 0;
        anim.fromValue = from;
        anim.toValue = to;
        anim.fillMode = kCAFillModeForwards;
        anim.isRemovedOnCompletion = false;
        
        add(anim, forKey: "progress")
    }
    
    func animationUpdatedTo(progress: CGFloat) {
        progressObserverClosure?(progress)
    }
    
}
