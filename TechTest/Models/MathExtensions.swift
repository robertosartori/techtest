//
//  MathExtensions.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import Foundation
import UIKit

class Math {
    
    /// Floor a float number to the highest integer even number less than the value
    ///
    /// - Parameter value: falue to be floored
    /// - Returns: floored even value
    static func floor2(_ value:Double)->Double {
        let floored = floor(value)
        return floored - Double((Int(floored) % 2))
    }

    /// Floor a float number to the highest integer even number less than the value
    ///
    /// - Parameter value: falue to be floored
    /// - Returns: floored even value
    static func floor2(_ value:CGFloat)->CGFloat {
        let floored = floor(value)
        return floored - CGFloat((Int(floored) % 2))
    }

}
