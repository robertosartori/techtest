//
//  ShopItem.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import Foundation
import UIKit

/// Represent a shop item
class ShopItem {
    
    /// Item unique ID
    let id : String
    
    /// Item name
    let name : String
    
    /// Item unity price
    let price : Price
    
    
    /// Primary initializer
    ///
    /// - Parameters:
    ///   - name: Item name
    ///   - price: Item price
    init(name:String, price:Price) {
        self.name = name
        self.price = price

        // note: valid only for demo
        self.id = name
    }
    
}
