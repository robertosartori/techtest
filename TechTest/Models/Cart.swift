//
//  Cart.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import Foundation

/// Shopping cart
class Cart {
    
    /// List of items and count
    private var items = [String : Int]()
    
    /// Returns the total cost of the cart by adding the cost of each item
    var totalPrice: Double {
        return items.reduce(0.0) {(result: Double, kv: (key: String, value:Int)) -> Double in
            var partial = result
            if let item = Shop.sharedInstance.getItemWith(name: kv.key) {
                partial += item.price.value * Double(kv.value) / item.price.unity;
            }
            return partial
        }
    }

    /// Insert one item to the cart
    ///
    /// - Parameter item: item to be inserted
    func insert(item:ShopItem) {
        if let itemCount = items[item.id] {
            items[item.id] = itemCount + 1
        } else {
            items[item.id] = 1
        }
    }
    
    /// Remove an item from the cart
    ///
    /// - Parameter item: item to be removed
    func remove(item:ShopItem) {
        if let itemCount = items[item.id] {
            if itemCount == 1 {
                items.removeValue(forKey: item.id)
            } else {
                items[item.id] = itemCount - 1
            }
        }
    }
    
    /// Returns the count of the give item
    ///
    /// - Parameter item: item
    /// - Returns: item count
    func countFor(item:ShopItem)->Int {
        if let count = items[item.id] {
            return count
        } else {
            return 0
        }
    }
    
    /// Remove al items form the cart
    func removeAllItems() {
        items.removeAll()
    }
    
}
