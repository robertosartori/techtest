//
//  Price.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import Foundation

struct Price {
    let value : Double
    let currency : String
    let unity : Double
    
    init(value:Double, currency:String, unity:Double) {
        self.value = value
        self.currency = currency
        self.unity = unity
    }
}
