//
//  Shop.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import Foundation

/// Shop descriptor
class Shop {

    /// Singleton instance
    static let sharedInstance = Shop()
    
    /// List of items contained into the shop
    private var items = [String : ShopItem]()
    
    /// Return the list of all items available in th shop
    var allItems : [ShopItem] {
        return Array(items.values)
    }
    
    init() {
        createShop()
    }
    
    /// Initialize the model
    func createShop() {
        // create the shop items. Hardcoded.
        addItem(item: ShopItem(name: "Peas", price: Price(value:0.95, currency:AppDefaultCurrency.defaultCurrency, unity: 1)))
        addItem(item: ShopItem(name: "Eggs", price: Price(value:2.10, currency:AppDefaultCurrency.defaultCurrency, unity: 12)))
        addItem(item: ShopItem(name: "Milk", price: Price(value:1.30, currency:AppDefaultCurrency.defaultCurrency, unity: 1)))
        addItem(item: ShopItem(name: "Beans", price: Price(value:0.73, currency:AppDefaultCurrency.defaultCurrency, unity: 1)))
    }
    
    /// Retrieve an item given its name
    ///
    /// - Parameter name: item name
    /// - Returns: the item if found
    func getItemWith(name: String) -> ShopItem? {
        return items[name]
    }
    
    /// Add an item to the shop
    ///
    /// - Parameter item: item to be added
    func addItem(item:ShopItem) {
        items[item.id] = item
    }
    
}
