//
//  CurrencyManager.swift
//  TechTest
//
//  Created by Roberto Sartori on 23/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import Foundation

class CurrencyManager {
    
    struct Quotes {
        var timestamp : TimeInterval = 0
        var currencies = [String: Double]()
    }
    
    /// last quotes available
    var quotes: Quotes = Quotes()
    
    /// Singleton manager
    static let shared = {
        return CurrencyManager()
    }
    
    init() {
        loadFromUserDefaults()
        fetchNewQuotes()
    }
    
    deinit {
        saveToUserdefaults()
    }
    
    // MARK: - Public
    
    /// Returns the number of quotes available
    ///
    /// - Returns: number of quotes
    func numberOfQuotes()->Int {
        return quotes.currencies.count
    }
    
    /// Retrieve the currency code and ratio at given index
    ///
    /// - Parameter index: position of the currency
    /// - Returns: currency code and ration
    func currencyAt(index:Int) -> (code: String, ratio:Double) {
        let keys = [String] (quotes.currencies.keys)
        let code = keys[index]
        return (code, quotes.currencies[code]!)
    }
    
    /// Formats the price
    ///
    /// - Parameters:
    ///   - price: price value
    ///   - isocode: valuta isocode
    /// - Returns: formatte price
    func priceFormatter(price: Double, isocode:String) -> String {
    
        // not trivial to decide the format since a single valuta code can have multiple locales
        let f = NumberFormatter()
        f.numberStyle = NumberFormatter.Style.currency
        f.currencySymbol = ""
        return (f.string(from: NSNumber(value: price)))!
    }
    
    // MARK: - Privates
    
    /// Fetch new quotes from backend
    private func fetchNewQuotes() {
        ApiLayerService.shared().fetchNewQuotes { (success:Bool, data:[String: Any]?) in
            if success {
                self.quotes.currencies = data?["quotes"] as! [String : Double]!
                let ts : TimeInterval = data?["timestamp"] as! TimeInterval
                self.quotes.timestamp = ts
            }
        }
    }
    
    /// Load previously stored quotes
    private func loadFromUserDefaults() {
        if let data = UserDefaults.standard.object(forKey: "CurrencyManager.currencies") {
            let unwrappedData = data as! [String : Double]
            
            // filter keys removing first 3 chars
            for key in unwrappedData.keys {
                let value = unwrappedData[key]

                let index = key.index(key.endIndex, offsetBy: -3)
                let newKey = key.substring(from: index)
            
                quotes.currencies[newKey] = value
            }
        }
        
        if let timestamp = UserDefaults.standard.object(forKey: "CurrencyManager.timestamp") {
            quotes.timestamp = timestamp as! Double
        }
    }
    
    /// Persist last quotes
    private func saveToUserdefaults() {
        UserDefaults.standard.setValue(quotes.currencies, forKey: "CurrencyManager.currencies")
        UserDefaults.standard.setValue(quotes.timestamp, forKey: "CurrencyManager.timestamp")
    }
    
}
