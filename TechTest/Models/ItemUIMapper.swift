//
//  ItemUIMapper.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import Foundation
import UIKit

class ItemUIMapper {
    
    /// Singleton instance
    static let sharedInstance = ItemUIMapper()
    
    /// Static map [item name -> image name] (for demo purposes)
    private var imageMap = ["Peas" : "peas_icon",
                            "Eggs" : "eggs_icon",
                            "Milk" : "milk_icon",
                            "Beans" : "beans_icon"]
    
    /// Static map [item name -> image name] (for demo purposes)
    private var placeholderColorMap = ["Peas" : UIColor(red: 187.0/255.0, green: 242.0/255.0, blue: 114/255.0, alpha: 1),
                                       "Eggs" : UIColor(red: 224.0/255.0, green:  90.0/255.0, blue:  79/255.0, alpha: 1),
                                       "Milk" : UIColor(red: 253.0/255.0, green: 179.0/255.0, blue:  46/255.0, alpha: 1),
                                       "Beans" : UIColor(red: 228.0/255.0, green: 162.0/255.0, blue: 134/255.0, alpha: 1)]
    
    /// Retriece the item icon
    ///
    /// - Parameter item: item
    /// - Returns: item icon image
    func imageFor(item:ShopItem) -> UIImage? {
        if let name = imageMap[item.name] {
            return UIImage(named: name)
        } else {
            return nil
        }
    }
    
    /// Returns the placeholder color fot the given item
    ///
    /// - Parameter item: item
    /// - Returns: placeholder color
    func placeholderColorFor(item:ShopItem) -> UIColor? {
        return placeholderColorMap[item.name]
    }
    
}
