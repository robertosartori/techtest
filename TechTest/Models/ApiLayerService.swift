//
//  ApiLayerService.swift
//  TechTest
//
//  Created by Roberto Sartori on 23/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import Foundation


/// This class is the interface to the ApiLayer services
class ApiLayerService {
    
    /// Normally securely encrypted, use in clear for testing
    let api_key = "c12acb8ecf9e0c2f8883a92ef897d91f"

    
    /// Singleton instance
    static let shared = {
        return ApiLayerService()
    }
    
    /// Fetch a new set of quotes
    ///
    /// - Parameter completionHandler: task termination handler
    func fetchNewQuotes(completionHandler: @escaping (Bool, [String : Any]?)->Void) {
        
        // construct the url
        
        let url = URL(string: "http://apilayer.net/api/live?access_key=\(api_key)")!
        
        let task = URLSession.shared.dataTask(with: url) { (data: Data?, response: URLResponse?, error:Error?) in
            // check errors first
            if error != nil {
                completionHandler(false, nil)
            }
            
            // parse response
            if let data = data {
                do {
                    let retVal = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
                    completionHandler(true, retVal)
                } catch {
                    completionHandler(false, nil)
                }
            }
            
        }
        task.resume()
    }
    
}
