//
//  AppDefaults.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import Foundation
import UIKit

/// Application font defaults
struct AppDefaultFonts {
    static let mainFontName = "HelveticaNeue"
    static let mainFontSize = CGFloat(18)
    static let mainFont = UIFont(name: AppDefaultFonts.mainFontName, size: AppDefaultFonts.mainFontSize)

    static let counterFontName = "HelveticaNeue-Light"
    static let counterFontSize = CGFloat(16)
    static let counterFont = UIFont(name: AppDefaultFonts.counterFontName, size: AppDefaultFonts.counterFontSize)
    
    static let priceFontName = "HelveticaNeue-Bold"
    static let priceFontSize = CGFloat(38)
    static let priceFont = UIFont(name: AppDefaultFonts.priceFontName, size: AppDefaultFonts.priceFontSize)

    static let placeholderFontName = "HelveticaNeue-Bold"
    
}

/// Currency defaults
struct AppDefaultCurrency {
    static let defaultCurrency = "USD"
}

/// Some geometry parameters
struct AppDefaultShoppingGeometry {
    /// irems displacement radius ratio with respect the shopping view width
    static let displacementRadiusRatio = CGFloat(0.86 / 2.0)
    
    /// size ratio of an item with respect the shopping view width
    static let itemSizeRatio = CGFloat(0.4)
    
    static let itemCounterPositionAngleOffset = M_PI + M_PI_2*1.3
    

}
