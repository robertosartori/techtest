//
//  ItemView.swift
//  TechTest
//
//  Created by Roberto Sartori on 21/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import UIKit

protocol ItemViewDelegate {
    func didTouchDownOnItemView(touchedView:ItemView)
    func didMoveItemView(movedView:ItemView, touchLocation:CGPoint)
    func didTouchUpOnItemView(touchedView:ItemView, touchLocation:CGPoint)
    func didCancelTouchOnItemView(touchedView:ItemView)
}

class ItemView: UIView {

    // MARK: - Properties
    
    /// determine the orientation of the displacement of elements (in radiants)
    var counterOrientation : CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    /// determine the main image ratio with respect the frame size
    var mainImgSizeRatio : CGFloat = 0.7

    /// determine the counter label size with respect the main image
    var counterSizeRatio : CGFloat = 0.4

    /// determine the displacement of the counter label's center along the displacement direction.  The ration is calculated with respect the main image size
    var counterCenterRatio : CGFloat = 1.2
    
    /// used to store the default orientation of the counter
    var counterDefaultOrientation : CGFloat = 0.0
    
    /// protocol delegation
    var delegate:ItemViewDelegate?
    
    /// color used to superimpose the placeholder circle dragged by the user pan gesture
    var palceholderColor : UIColor?
    
    // MARK: - Outlets
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemCounterView: ScrollingCounterView!

    // MARK : Privates
    
    /// placeholder label used during user intraction
    private var placeholder : UILabel?
    
    /// Touch management
    private var initialTouchLocation : CGPoint = CGPoint(x: 0, y: 0)
    
    /// counter position animation timer
    private var counterAnimationTimer : Timer?
    
    /// counter final position value used during animation
    private var counterAnimationFinalValue : CGFloat = 0
    
    // MARK: - Lifecycle
    
    /// Load an instance from teh Nib
    ///
    /// - Returns: the item view
    static func instanceFromNib() -> ItemView {
        return UINib(nibName: "ItemView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ItemView
    }

    /// Basic initialization
    func initialize() {
        self.itemImageView.layer.shadowColor = UIColor.black.cgColor
        self.itemImageView.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.itemImageView.layer.shadowOpacity = 0
        self.itemImageView.layer.shadowRadius = 3
        self.itemImageView.layer.shouldRasterize = true
        
        self.itemCounterView.layer.masksToBounds = true;
        self.itemCounterView.layer.borderWidth = 1;
        self.itemCounterView.layer.borderColor = UIColor.lightGray.cgColor
        self.itemCounterView.layer.shouldRasterize = true
        
        // attach a long press recognizer with 0 delay time in order to capture immediatly the touchdown, track the pan and the touch up events
        let touchGesture = UILongPressGestureRecognizer(target: self, action: #selector(touchGestureRecognized))
        touchGesture.minimumPressDuration = 0
        addGestureRecognizer(touchGesture)
        
        self.layer.shouldRasterize = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateFrameSizes()
    }
    
    // MARK: - Public UI/UX Management
    
    /// Create and activate the placeholder, animated
    func activatePlaceholder() {
        createThePlaceholder()
        let T = CGAffineTransform(scaleX: 1.04, y: 1.04)
        UIView.animate(withDuration: 0.01) {
            self.itemImageView.transform = T
            self.placeholder?.transform = T
            self.itemImageView.layer.shadowOpacity = 0.5
            
        }
        isUserInteractionEnabled = true
    }
    
    /// Assign the placeholder title
    ///
    /// - Parameter title: paceholder title
    func setPlaceholder(title:String) {
        placeholder?.text = title
    }

    /// Animate to the deactivate state
    func deactivateView() {
        UIView.animate(withDuration: 0.12) {
            self.alpha = CGFloat(0.4)
            self.itemImageView.transform = CGAffineTransform(scaleX: 0.98, y: 0.98)
            self.placeholder?.alpha = 0;
        }
        
        UIView.animate(withDuration: 0.04, animations: {
            self.alpha = CGFloat(0.4)
            self.itemImageView.transform = CGAffineTransform(scaleX: 0.98, y: 0.98)
            self.placeholder?.alpha = 0;
        }) { (finished:Bool) in
            self.placeholder?.removeFromSuperview()
        }
        isUserInteractionEnabled = false
    }

    /// Restore the normal view appearance
    func resetView() {
        UIView.animate(withDuration: 0.04) {
            self.alpha = 1.0
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.itemImageView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.itemImageView.layer.shadowOpacity = 0
        }
        isUserInteractionEnabled = true
        dismissPlaceholder()
    }

    /// Set the new counter value, animated
    ///
    /// - Parameter value: the new counter value
    func animateCounterTo(value: Int) {
        itemCounterView.animateTo(value: value)
    }
    
    /// Position the center of the counter view starting from the item center in a new postion along the direction given by the angle. This chenge is being animated.
    ///
    /// - Parameter value: the angle that determine the counter view's center
    func animateCounerAngleTo(value: CGFloat) {
        if counterAnimationTimer == nil {
            counterAnimationTimer = Timer.scheduledTimer(timeInterval: 0.02, target: self, selector: #selector(animateCounterCallback), userInfo: nil, repeats: true)
        }
        counterAnimationFinalValue = value
    }
    
    /// Update the counter postion acconring to the current orientation value
    func upateCounterPosition() {
        setCounterPositionWith(orientation: counterOrientation)
    }

    // MARK: - Private UX
    
    /// This callback implements the animation that moves the counter view around the item view to reach the target angle
    @objc private func animateCounterCallback() {
        let delta = Math.floor2((counterAnimationFinalValue - counterOrientation) * 400.0) / 400.0
        if delta == 0 {
            counterAnimationTimer?.invalidate()
            counterAnimationTimer = nil
        } else {
            let sat = CGFloat(M_PI_2/4.0)
            counterOrientation += max(-sat, min(sat, delta * 0.4))
            setCounterPositionWith(orientation: counterOrientation)
        }
    }
    
    /// Position the counter given the angle from the center of the item immediatly, without animations
    ///
    /// - Parameter orientation: the angle that defines the counter center direction
    private func setCounterPositionWith(orientation:CGFloat) {
        let referenceSize = Math.floor2(self.frame.width * CGFloat(mainImgSizeRatio))
        itemCounterView.center = CGPoint(x: Math.floor2(itemImageView.center.x + referenceSize * CGFloat(counterCenterRatio / 2.0 * cos(orientation))),
                                         y: Math.floor2(itemImageView.center.x + referenceSize * CGFloat(counterCenterRatio / 2.0 * sin(orientation))))
    }

    /// Update the size of the frames with respect the whole container
    private func updateFrameSizes() {
        let referenceSize = Math.floor2(self.frame.width * CGFloat(mainImgSizeRatio))
        // main image
        itemImageView.bounds = CGRect(x: 0,
                                      y: 0,
                                      width: referenceSize,
                                      height: referenceSize)
        itemImageView.center = CGPoint(x: self.frame.size.width / 2.0,
                                       y: self.frame.size.height / 2.0)
        
        // counter label
        itemCounterView.bounds = CGRect(x: 0,
                                        y: 0,
                                        width: Math.floor2(referenceSize * CGFloat(counterSizeRatio)),
                                        height: Math.floor2(referenceSize * CGFloat(counterSizeRatio)))
        setCounterPositionWith(orientation: counterOrientation)
        // update drawing parameters coupled with geometries
        self.itemCounterView.layer.cornerRadius = self.itemCounterView.bounds.size.width/4.0;
    }

    /// Create the placeholder view and add to view hierarchy
    private func createThePlaceholder() {
        placeholder = UILabel()
        addSubview(placeholder!)
        placeholder?.font = UIFont(name: AppDefaultFonts.placeholderFontName, size: 44)
        placeholder?.textColor = UIColor.white
        placeholder?.textAlignment = .center
        placeholder?.frame = itemImageView.frame
        placeholder?.backgroundColor = palceholderColor ?? UIColor.black
        var h : CGFloat = 0
        var s : CGFloat = 0
        var b : CGFloat = 0
        var a : CGFloat = 0
        palceholderColor?.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        placeholder?.layer.borderColor = UIColor(hue: h, saturation: s, brightness: b/2.0, alpha: 0.8).cgColor
        placeholder?.layer.cornerRadius = itemImageView.frame.size.width / 2.0
        placeholder?.layer.borderWidth = 2
        placeholder?.layer.masksToBounds = true
        placeholder?.alpha = 0.8
    }
    
    /// dismiss the placeholder, animated
    private func dismissPlaceholder() {
        if let localPlaceholder = placeholder {
            self.placeholder = nil
            UIView.animate(withDuration: 0.16, animations: {
                localPlaceholder.alpha = 0;
                localPlaceholder.layer.borderWidth = 0
            }) { (finished:Bool) in
                localPlaceholder.removeFromSuperview()
            }
        }
    }

    
    // MARK: - Gesture recognizer delegate
    @objc private func touchGestureRecognized(gestureRecognizer: UILongPressGestureRecognizer) {
        
        let touchLocation = gestureRecognizer.location(in: self)
        let relativeTouchLocation = CGPoint(x: touchLocation.x - initialTouchLocation.x,
                                            y: touchLocation.y - initialTouchLocation.y)

        switch gestureRecognizer.state {
        case .began:
            initialTouchLocation = gestureRecognizer.location(in: self)
            delegate?.didTouchDownOnItemView(touchedView: self)
            break
        case .cancelled:
            delegate?.didCancelTouchOnItemView(touchedView: self)
            break
        case .changed:
            delegate?.didMoveItemView(movedView: self, touchLocation: relativeTouchLocation)
            self.placeholder?.center = CGPoint(x: itemImageView.center.x + relativeTouchLocation.x,
                                               y: itemImageView.center.y + relativeTouchLocation.y)
            break
        case .ended:
            delegate?.didTouchUpOnItemView(touchedView: self, touchLocation: relativeTouchLocation)
            break
        case.failed:
            delegate?.didCancelTouchOnItemView(touchedView: self)
            break
        case.possible:
            break
        }
    }
}
