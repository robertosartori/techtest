//
//  ScrollingCounterView.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import UIKit

class ScrollingCounterView: UIView {
    
    /// internal counter
    private var _value = 0
    
    /// Used to show the curent value
    private var currentValueLabel: UILabel = UILabel()
    
    /// Uset to show the next value during animation
    private var nextValueLabel: UILabel = UILabel()
    
    /// counter font
    private var font = AppDefaultFonts.counterFont
    
    private var currentLabel = UILabel()
    private var nextLabel = UILabel()
    
    // MARK: - Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initLabel(label: UILabel) {
        label.font = self.font
        label.tintColor = UIColor.black
        label.textAlignment = NSTextAlignment.center
    }
    
    private func initialize() {
        currentLabel = UILabel()
        nextLabel = UILabel()
        initLabel(label: currentLabel)
        initLabel(label: nextLabel)
        
        addSubview(currentLabel)
        addSubview(nextLabel)
        setCounter(value: 0)
    }
    
    override func layoutSubviews() {
        currentLabel.frame = bounds
        nextLabel.frame = bounds
    }
    
    // MARK: - Public
    
    /// Set the counter value immediatly without animations
    ///
    /// - Parameter value: desired counter value
    func setCounter(value:Int) -> Void {
        _value = value
        
        currentLabel.text = "\(_value)"
        currentLabel.alpha = 1
        
        nextLabel.alpha = 0
        nextLabel.frame = currentLabel.frame
        
        layoutSubviews()
    }
    
    /// Perform a scroll animation to the given value
    ///
    /// - Parameter value: final value
    func animateTo(value: Int) -> Void {
        if value > _value {
            increaseTo(value)
        } else if value < _value {
            decreaseTo(value)
        }
    }
    
    // MARK: - Private UX
    
    private func increaseTo(_ value: Int) {
        nextLabel.alpha = 1
        nextLabel.text = "\(value)"
        nextLabel.center = CGPoint(x: currentLabel.center.x, y: currentLabel.center.y + currentLabel.bounds.size.height)
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.nextLabel.center = self.currentLabel.center
                        self.currentLabel.center = CGPoint(x: self.currentLabel.center.x, y: self.currentLabel.center.y - self.currentLabel.bounds.size.height)
        }) { (completed:Bool) in
            self.setCounter(value: value)
        }
    }
    
    private func decreaseTo(_ value: Int) {
        nextLabel.alpha = 1
        nextLabel.text = "\(value)"
        nextLabel.center = CGPoint(x: currentLabel.center.x, y: currentLabel.center.y - currentLabel.bounds.size.height)
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.nextLabel.center = self.currentLabel.center
                        self.currentLabel.center = CGPoint(x: self.currentLabel.center.x, y: self.currentLabel.center.y + self.currentLabel.bounds.size.height)
        }) { (completed:Bool) in
            self.setCounter(value: value)
        }
    }
    
}
