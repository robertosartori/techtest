//
//  AnimatedRings.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import UIKit

class AnimatedRings: UIView {

    struct Ring {
        var deltaRadius : CGFloat = 1.0
        var deltaRadiusPeriod : CGFloat = 1.0
    }
    
    private var animationTime : Double = 3.14159 * 2.71828
    
    private var rings = [Ring]()
    
    var ringSize : CGFloat = 0.0
    var grayColor : CGFloat = 0.95
    
    private var animationTimer : Timer?
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        // Drawing code
        for ring in rings {
            let cosValue = CGFloat(cos(ring.deltaRadiusPeriod * CGFloat(animationTime)))
            let currentRadius = CGFloat(ringSize + ringSize * ring.deltaRadius * cosValue)
            let insets = (self.bounds.size.width - currentRadius) / 2.0
            let circle = UIBezierPath(ovalIn: UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(insets, insets, insets, insets)))
            
            let alpha = (1.0 - cosValue*cosValue)
            let color = alpha*grayColor + (1.0 - alpha)
            
            UIColor(white: CGFloat(color), alpha: alpha).setStroke()
            circle.stroke()
        }
    }
    
    func initialize() {
        
        for _ in 1...60 {
            var r = Ring()
            r.deltaRadius = 0.1 + CGFloat(arc4random_uniform(100)) / 1000.0
            r.deltaRadiusPeriod = CGFloat(arc4random_uniform(100)) / 1500.0
            rings.append(r)
        }
    }

    func startAnimating() {
        animationTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(updateDrawing), userInfo: nil, repeats: true)
    }
    
    func stopAnimating() {
        animationTimer?.invalidate()
    }
    
    func updateDrawing() {
        animationTime += 0.2
        setNeedsDisplay()
    }
}
