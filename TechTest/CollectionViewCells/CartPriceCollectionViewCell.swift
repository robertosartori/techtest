//
//  CartPriceCollectionViewCell.swift
//  TechTest
//
//  Created by Roberto Sartori on 23/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import UIKit

class CartPriceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
}
