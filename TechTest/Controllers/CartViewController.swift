//
//  CartViewController.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import UIKit

class CartViewController: UIViewController, ItemViewDelegate {

    // MARK: - Private
    /// list of item views
    private var itemViews = [ItemView]()
    
    /// Cart
    private var cart = Cart()
    
    /// circular sector the items are distributed in
    private var itemDistributionCurrentSector : Double = 0
    
    /// circular sector target for animating the items poisition change animation
    private var itemDistributionAnimationToSector : Double = 0
    
    /// The possible states of the cart operation
    ///
    /// - neutral: no operation is required
    /// - addItem: the item has to be added to the cart
    /// - removeItem: the item has to be removed from the cart
    enum CartOperation {
        case neutral
        case addItem
        case removeItem
    }
    
    
    /// The available layouts of the view
    ///
    /// - shopping: shopping layout used when user is feeding the cart
    /// - checkout: the checkout layout used to inspect the cart total cost
    enum Layout {
        case shopping
        case checkout
    }
    
    /// the current visible layout
    private var layout : Layout = .shopping
    
    /// lazy loading dispatch once pattern
    private lazy var updateLayoutOnce : () = {
        self.updateLayouts()
    }()
    
    // MARK: - Outlets
    @IBOutlet weak var animationRings: AnimatedRings!
    @IBOutlet weak var shoppingCartImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    
    // MARK: - Lyfecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // initialize views and data
        initializeShopping()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // first initialization here
        animationRings.startAnimating()
        updateLayouts()
    }
    
    override func viewDidLayoutSubviews() {
        _ = updateLayoutOnce
    }
    
    // MARK: - Public
    
    /// Remove all items from the cart. Update the model and the view accordingly
    func removeAllItemsInCart() {
        // clean up the cart model
        cart.removeAllItems()
        
        // update all views
        for view in itemViews {
            view.animateCounterTo(value: 0)
        }
        
        // restore cart image, animated
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       usingSpringWithDamping: 0.3,
                       initialSpringVelocity: 10.0,
                       options: .curveEaseInOut,
                       animations: {
                        self.shoppingCartImageView.transform = CGAffineTransform.identity
        }) { (completed:Bool) in
            
        }
    }
    
    /// Animate the cart in order to prepare tue user to the next action: all items will be removed
    func prepareToRemoteAllItemsInCart() {
        // animate the cart
        UIView.animate(withDuration: 0.12,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.0,
                       options: .curveEaseIn,
                       animations: {
                        self.shoppingCartImageView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8).rotated(by: 0.2)
        }) { (completed:Bool) in
        }
    }
    
    /// Animate the Cart layout to the checkout case
    func aniamteToCheckoutLayout() {
        // update model
        layout = .checkout
        
        // stop animating rings
        self.animationRings.stopAnimating()

        // update cart price
        refreshCartPrice()
        
        // lock interaction
        view.isUserInteractionEnabled = false
        
        // expand rings and make them disappear
        self.priceLabel.transform = CGAffineTransform(translationX: 0, y: -20)
        UIView.animate(withDuration: 1,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0,
                       options: .curveEaseInOut,
                       animations: {
                        // expand ring view outside the screen
                        self.animationRings.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                        self.animationRings.alpha = 0
                        
        }) { (finished:Bool) in            
        }
        
        UIView.animate(withDuration: 0.8,
                       delay: 0.4,
                       usingSpringWithDamping: 0.66,
                       initialSpringVelocity: 0,
                       options: .curveEaseInOut,
                       animations: {
                        // price label
                        self.priceLabel.transform = CGAffineTransform.identity
                        self.priceLabel.alpha = 1
                        
        }) { (finished:Bool) in
        }

        // animate counters position on the bottom of each item
        updateCounterPosition(animated: true)
        
        // distribute items along a decreasing sector
        Timer.scheduledTimer(timeInterval: 0.02, target: self, selector: #selector(itemsDistributionAnimationCallback(timer:)), userInfo: nil, repeats: true)
        itemDistributionAnimationToSector = M_PI
    }

    /// Animate the Cart layout to the shopping case
    func animateToShoppingLayout() {
        // update model
        layout = .shopping
        
        // lock interaction
        view.isUserInteractionEnabled = true

        // restore ring original size
        UIView.animate(withDuration: 1,
                       delay: 0.4,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0,
                       options: .curveEaseInOut,
                       animations: {
                        // expand ring view outside the screen
                        self.animationRings.transform = CGAffineTransform.identity
                        self.animationRings.alpha = 1

        }) { (finished:Bool) in
            // restart animating rings
            self.animationRings.startAnimating()
        }
        
        UIView.animate(withDuration: 0.6,
                       delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0,
                       options: .curveEaseInOut,
                       animations: {
                        // price label
                        self.priceLabel.transform = CGAffineTransform(translationX: 0, y: -20)
                        self.priceLabel.alpha = 0
        }) { (finished:Bool) in
            
        }
        
        // distribute items around the cart again
        Timer.scheduledTimer(timeInterval: 0.02, target: self, selector: #selector(itemsDistributionAnimationCallback(timer:)), userInfo: nil, repeats: true)
        itemDistributionAnimationToSector = M_PI * 2.0

        // animate counters
        animateCounterToDefaultOrientation()
    }
    
    
    /// Refreshes the cart price
    func refreshCartPrice() {
        let price = cart.totalPrice
        priceLabel.text = CurrencyManager.shared().priceFormatter(price: price, isocode: "USD")
    }
    
    /// Return the cart price in USD
    ///
    /// - Returns: cart total price
    func currentCartPrice()->Double {
        return cart.totalPrice
    }
    
    // MARK: - UX Aux
    
    /// Update the layout according the current view size
    private func updateLayouts() {
        // refresh frame sizes
        for itemView in itemViews {
            itemView.bounds = CGRect(x: 0,
                                     y: 0,
                                     width: Math.floor2(AppDefaultShoppingGeometry.itemSizeRatio * view.bounds.size.width),
                                     height: Math.floor2(AppDefaultShoppingGeometry.itemSizeRatio * view.bounds.size.width))
        }
        
        // distribute items around the cart
        distributeItemsOn(sector: itemDistributionCurrentSector)
        
        // counter postions
        updateCounterPosition(animated: false, storeDefaultOrinetation: true)
        
        /// items displacement radius
        let radius = self.view.bounds.width * CGFloat(AppDefaultShoppingGeometry.displacementRadiusRatio)
        
        self.animationRings.ringSize = radius * 2.0
        self.animationRings.transform = CGAffineTransform.identity
        self.animationRings.alpha = 1
        
        // price is hiden at the beginning
        priceLabel.alpha = 0
    }

    /// Updates the counter position acconrding to the current layout and orientation value
    ///
    /// - Parameter animated: true if update should be animated
    private func updateCounterPosition(animated:Bool) {
        updateCounterPosition(animated: animated, storeDefaultOrinetation:false)
    }
    
    
    /// Updates the counter position acconrding to the current layout and orientation value and save the current orientation as default if required
    ///
    /// - Parameters:
    ///   - animated: true if update should be animated
    ///   - storeDefaultOrinetation: true if current computed orientation should be saved as default
    private func updateCounterPosition(animated:Bool, storeDefaultOrinetation: Bool) {
        // counter disposal
        for itemView in itemViews {
            let angle = atan2(itemView.center.x - view.bounds.size.width/2, itemView.center.y - view.bounds.size.height/2)
            var orientation : CGFloat
            // update the counter position
            if layout == .shopping {
                orientation = -angle + CGFloat(M_PI_2)
            } else {
                orientation = CGFloat(M_PI_2)
            }
            if animated {
                itemView.animateCounerAngleTo(value: orientation)
            } else {
                itemView.counterOrientation = orientation
                itemView.upateCounterPosition()
            }
            
            if storeDefaultOrinetation {
                itemView.counterDefaultOrientation = orientation
            }
        }
    }
    
    /// Animates the counter position to the default orientation
    private func animateCounterToDefaultOrientation() {
        // counter disposal
        for itemView in itemViews {
            itemView.animateCounerAngleTo(value: itemView.counterDefaultOrientation)
        }
    }
    
    /// This function is used by the animation timer that manages the animated movement of the elements along a circular sector
    ///
    /// - Parameter timer: the animation timer
    @objc private func itemsDistributionAnimationCallback(timer: Timer) {
        // round in order to terminate the animation in a finite time
        let delta = Math.floor2((itemDistributionAnimationToSector - itemDistributionCurrentSector) * 400.0) / 400.0
        if delta == 0 {
            timer.invalidate()
        } else {
            let sat = M_PI_2/4.0
            itemDistributionCurrentSector += max(-sat, min(sat, delta * 0.4)) // <- saturated first-order response
            distributeItemsOn(sector: itemDistributionCurrentSector)
        }
    }

    /// Initialize the internal data model and the subviews
    private func initializeShopping() {
        // cycle through the shop items and setup an item view for each
        for item in Shop.sharedInstance.allItems {
            let itemView = ItemView.instanceFromNib()
            itemView.initialize()
            
            itemView.itemImageView.image = ItemUIMapper.sharedInstance.imageFor(item: item)
            itemView.palceholderColor = ItemUIMapper.sharedInstance.placeholderColorFor(item: item)
            itemView.itemCounterView.setCounter(value: 0)
            itemView.delegate = self
            
            // link the model to the view
            itemView.tag = itemViews.count
            
            itemViews.append(itemView)
            view.addSubview(itemView)
            view.insertSubview(itemView, aboveSubview: shoppingCartImageView)
        }
        
        // initialize rings animation
        animationRings.initialize()
        
        // start with all object distributed around the cart
        itemDistributionCurrentSector = M_PI * 2.0
        
        // setup price label
        priceLabel.font = AppDefaultFonts.priceFont
        priceLabel.textColor = UIColor.black
    }
    
    /// This function dispose the items around the cart concerning only the angular sector given (starting from zero to the sector value)
    ///
    /// - Parameter sector: the angular sector items have to be distributed to
    private func distributeItemsOn(sector:Double) {
        /// angle spanned between items
        let delta = -sector / Double(self.itemViews.count)
        
        /// starting direction from shopping cart to the item
        var angle = delta / 2.0
        
        /// items displacement radius
        let radius = self.view.bounds.width * CGFloat(AppDefaultShoppingGeometry.displacementRadiusRatio)
        
        for itemView in self.itemViews {
            // determine the center
            itemView.center = CGPoint(x: Math.floor2(self.view.bounds.width/2.0 + radius * CGFloat(cos(angle))),
                                      y: Math.floor2(self.view.bounds.height/2.0 + radius * CGFloat(sin(angle))))
            
            angle += delta
        }
    }

    
    // MARK: - ItemView Delegate
    func didTouchDownOnItemView(touchedView: ItemView) {
        // obscure and deactivate all other views
        for itemView in itemViews {
            if itemView != touchedView {
                itemView.deactivateView()
            } else {
                itemView.activatePlaceholder()
            }
        }
        // stop animation to let the user interaction to be smooth
        animationRings.stopAnimating()
    }

    func cartOperationFrom(view: ItemView, touchLocation: CGPoint) -> CartOperation {
        // compute the normalized distance
        let locationInView = CGPoint(x:view.center.x + touchLocation.x, y:view.center.y + touchLocation.y)
        let touchDistance = (locationInView.x - self.view.center.x)*(locationInView.x - self.view.center.x) + (locationInView.y - self.view.center.y)*(locationInView.y - self.view.center.y)
        let viewCenterDistance = (view.center.x - self.view.center.x)*(view.center.x - self.view.center.x) + (view.center.y - self.view.center.y)*(view.center.y - self.view.center.y)
        let ratio = touchDistance / viewCenterDistance

        if ratio < 0.8 {
            return .addItem
        } else if ratio > 1.2 {
            return .removeItem
        } else {
            return .neutral
        }
    }
    
    func didMoveItemView(movedView view: ItemView, touchLocation: CGPoint) {
        
        let operation = cartOperationFrom(view: view, touchLocation: touchLocation)

        switch operation {
        case .addItem:
            view.setPlaceholder(title: "+1")
            break
        case .removeItem:
            view.setPlaceholder(title: "-1")
            break
        case .neutral:
            view.setPlaceholder(title: "")
        }
    }
    
    func didTouchUpOnItemView(touchedView view: ItemView, touchLocation: CGPoint) {
        // obscure and deactivate all other views
        for itemView in itemViews {
            itemView.resetView()
        }
        
        // update the model
        let operation = cartOperationFrom(view: view, touchLocation: touchLocation)
        let shopItem = Shop.sharedInstance.allItems[view.tag]
        
        switch operation {
        case .addItem:
            cart.insert(item: shopItem)
            break
        case .removeItem:
            cart.remove(item: shopItem)
            break
        default:
            break
        }
        
        // animate
        view.animateCounterTo(value: cart.countFor(item: shopItem))
    
        // stop animation to let the user interaction to be smooth
        animationRings.stopAnimating()
    }
    
    func didCancelTouchOnItemView(touchedView: ItemView) {
        // obscure and deactivate all other views
        for itemView in itemViews {
            itemView.resetView()
        }

        // stop animation to let the user interaction to be smooth
        animationRings.stopAnimating()
    }
    
}
