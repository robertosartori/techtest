//
//  ShoppingViewController.swift
//  TechTest
//
//  Created by Roberto Sartori on 22/01/2017.
//  Copyright © 2017 Roberto Sartori. All rights reserved.
//

import UIKit

class ShoppingViewController: UIViewController {
    
    // MARK: - Private
    /// Cart View Controller
    private var cartVC : CartViewController?
    
    /// cart price currencies
    private var cartPriceVC : CartPriceCollectionViewController? = nil
    
    // MARK: - Outlets
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var clearCartButton: UIButton!
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var backToCartButton: UIButton!

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.backToCartButton.alpha = 0
        view.sendSubview(toBack: self.backToCartButton)
        
        // load cart prices
        cartPriceVC = self.storyboard?.instantiateViewController(withIdentifier: "CartPricesVC") as? CartPriceCollectionViewController
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "CartVC" {
            cartVC = segue.destination as? CartViewController
        }
    }
    
    // MARK: - Actions
    
    /// Go back button touch down from the checkout layout
    ///
    /// - Parameter sender: the button
    @IBAction func backToCart(_ sender: UIButton) {
        // setup cart shopping layout
        cartVC?.animateToShoppingLayout()
        
        // restore shopping layout buttons
        UIView.animate(withDuration: 0.4) {
            self.backToCartButton.alpha = 0
            self.backToCartButton.transform = CGAffineTransform(translationX: 0, y: 100)
            
            self.clearCartButton.transform = CGAffineTransform.identity
            self.checkoutButton.transform = CGAffineTransform.identity
            
            self.clearCartButton.alpha = 1
            self.checkoutButton.alpha = 1
            
        }

        UIView.animate(withDuration: 0.4,
                       delay: 0.2,
                       options: .beginFromCurrentState,
                       animations: { 
                        
                        // restore cart original position
                        self.cartView.transform = CGAffineTransform.identity

        }, completion: nil)
        
        UIView.animate(withDuration: 0.4,
                       delay: 0.2,
                       options: .beginFromCurrentState,
                       animations: {
                        
                        // fade out prices
                        self.cartPriceVC?.view.alpha = 0
                        
        }, completion: { (finished:Bool) in
            self.cartPriceVC?.view.removeFromSuperview()
        })
    }
    
    /// Go to checkout button press from the shopping layout
    ///
    /// - Parameter sender: the button
    @IBAction func checkout(_ sender: UIButton) {

        // setup cart checkout layout
        self.cartVC?.aniamteToCheckoutLayout()
        
        // animate to checkout buttons layout
        self.backToCartButton.transform = CGAffineTransform(translationX: 0, y: 100)
        UIView.animate(withDuration: 0.4) {
            self.backToCartButton.alpha = 1
            self.backToCartButton.transform = CGAffineTransform.identity
            
            self.clearCartButton.transform = CGAffineTransform(translationX: -200, y: 0)
            self.checkoutButton.transform = CGAffineTransform(translationX: 200, y: 0)
            
            self.clearCartButton.alpha = 0
            self.checkoutButton.alpha = 0
            
            // move cart up to make space for foreign quotes
            self.cartView.transform = CGAffineTransform(translationX: 0, y: -Math.floor2(self.cartView.frame.origin.y / 2.0))
        }
        self.view.bringSubview(toFront: self.backToCartButton)
        
        // compute the cart price
        let top = self.view.convert(CGPoint(x: 0, y: self.cartView.bounds.size.height), from:self.cartView)
        let bottom = self.view.convert(CGPoint(x: 0, y: 0), from:self.backToCartButton)
        
        self.view.addSubview((self.cartPriceVC?.view)!)
        self.cartPriceVC?.view.frame = CGRect(x: 10, y: top.y, width: self.view.bounds.size.width - 20, height: bottom.y - top.y - 8)
        self.cartPriceVC?.referencePrice = (self.cartVC?.currentCartPrice())!
        self.cartPriceVC?.view.alpha = 0
        
        UIView.animate(withDuration: 0.4,
                       delay: 0.4,
                       options:.beginFromCurrentState,
                       animations: {
                        self.cartPriceVC?.view.alpha = 1
        }) { (finished: Bool) in
            self.cartPriceVC?.collectionView?.reloadItems(at: (self.cartPriceVC?.collectionView?.indexPathsForVisibleItems)!)
        }
    }
    
    /// Clear cart button touch down, a first animation starts here
    ///
    /// - Parameter sender: the button
    @IBAction func prepareToClearCart(_ sender: UIButton) {
        cartVC?.prepareToRemoteAllItemsInCart()
    }
    
    /// Clear cart button touch up, all items in the cart are removed
    ///
    /// - Parameter sender: the button
    @IBAction func removeAllItemsInCart(_ sender: UIButton) {
        cartVC?.removeAllItemsInCart()
    }

}
